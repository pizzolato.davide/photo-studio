﻿namespace _32.Photo_Studio
{
    partial class RGBSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trkBar_red = new System.Windows.Forms.TrackBar();
            this.nUpDw_red = new System.Windows.Forms.NumericUpDown();
            this.btt_applica = new System.Windows.Forms.Button();
            this.nUpDw_green = new System.Windows.Forms.NumericUpDown();
            this.trkBar_green = new System.Windows.Forms.TrackBar();
            this.nUpDw_blue = new System.Windows.Forms.NumericUpDown();
            this.trkBar_blue = new System.Windows.Forms.TrackBar();
            this.trkBar_alpha = new System.Windows.Forms.TrackBar();
            this.nUpDw_alpha = new System.Windows.Forms.NumericUpDown();
            this.lbl_alpha = new System.Windows.Forms.Label();
            this.lbl_red = new System.Windows.Forms.Label();
            this.lbl_green = new System.Windows.Forms.Label();
            this.lbl_blue = new System.Windows.Forms.Label();
            this.btt_undo = new System.Windows.Forms.PictureBox();
            this.btt_redo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.trkBar_red)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDw_red)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDw_green)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkBar_green)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDw_blue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkBar_blue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkBar_alpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDw_alpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_undo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_redo)).BeginInit();
            this.SuspendLayout();
            // 
            // trkBar_red
            // 
            this.trkBar_red.Location = new System.Drawing.Point(12, 100);
            this.trkBar_red.Maximum = 255;
            this.trkBar_red.Minimum = -255;
            this.trkBar_red.Name = "trkBar_red";
            this.trkBar_red.Size = new System.Drawing.Size(260, 45);
            this.trkBar_red.TabIndex = 0;
            this.trkBar_red.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trkBar_red.Scroll += new System.EventHandler(this.trkBar_red_Scroll);
            // 
            // nUpDw_red
            // 
            this.nUpDw_red.Location = new System.Drawing.Point(278, 100);
            this.nUpDw_red.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nUpDw_red.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.nUpDw_red.Name = "nUpDw_red";
            this.nUpDw_red.Size = new System.Drawing.Size(57, 20);
            this.nUpDw_red.TabIndex = 1;
            this.nUpDw_red.ValueChanged += new System.EventHandler(this.nUpDw_red_ValueChanged);
            // 
            // btt_applica
            // 
            this.btt_applica.Location = new System.Drawing.Point(260, 271);
            this.btt_applica.Name = "btt_applica";
            this.btt_applica.Size = new System.Drawing.Size(75, 23);
            this.btt_applica.TabIndex = 2;
            this.btt_applica.Text = "Applica";
            this.btt_applica.UseVisualStyleBackColor = true;
            this.btt_applica.Click += new System.EventHandler(this.btt_applica_Click);
            // 
            // nUpDw_green
            // 
            this.nUpDw_green.Location = new System.Drawing.Point(278, 161);
            this.nUpDw_green.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nUpDw_green.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.nUpDw_green.Name = "nUpDw_green";
            this.nUpDw_green.Size = new System.Drawing.Size(57, 20);
            this.nUpDw_green.TabIndex = 4;
            this.nUpDw_green.ValueChanged += new System.EventHandler(this.nUpDw_green_ValueChanged);
            // 
            // trkBar_green
            // 
            this.trkBar_green.Location = new System.Drawing.Point(12, 161);
            this.trkBar_green.Maximum = 255;
            this.trkBar_green.Minimum = -255;
            this.trkBar_green.Name = "trkBar_green";
            this.trkBar_green.Size = new System.Drawing.Size(260, 45);
            this.trkBar_green.TabIndex = 3;
            this.trkBar_green.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trkBar_green.Scroll += new System.EventHandler(this.trkBar_green_Scroll);
            // 
            // nUpDw_blue
            // 
            this.nUpDw_blue.Location = new System.Drawing.Point(278, 222);
            this.nUpDw_blue.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nUpDw_blue.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.nUpDw_blue.Name = "nUpDw_blue";
            this.nUpDw_blue.Size = new System.Drawing.Size(57, 20);
            this.nUpDw_blue.TabIndex = 6;
            this.nUpDw_blue.ValueChanged += new System.EventHandler(this.nUpDw_blue_ValueChanged);
            // 
            // trkBar_blue
            // 
            this.trkBar_blue.Location = new System.Drawing.Point(12, 222);
            this.trkBar_blue.Maximum = 255;
            this.trkBar_blue.Minimum = -255;
            this.trkBar_blue.Name = "trkBar_blue";
            this.trkBar_blue.Size = new System.Drawing.Size(260, 45);
            this.trkBar_blue.TabIndex = 5;
            this.trkBar_blue.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trkBar_blue.Scroll += new System.EventHandler(this.trkBar_blue_Scroll);
            // 
            // trkBar_alpha
            // 
            this.trkBar_alpha.Location = new System.Drawing.Point(12, 39);
            this.trkBar_alpha.Maximum = 255;
            this.trkBar_alpha.Minimum = -255;
            this.trkBar_alpha.Name = "trkBar_alpha";
            this.trkBar_alpha.Size = new System.Drawing.Size(260, 45);
            this.trkBar_alpha.TabIndex = 7;
            this.trkBar_alpha.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trkBar_alpha.Scroll += new System.EventHandler(this.trkBar_alpha_Scroll);
            // 
            // nUpDw_alpha
            // 
            this.nUpDw_alpha.Location = new System.Drawing.Point(278, 39);
            this.nUpDw_alpha.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nUpDw_alpha.Minimum = new decimal(new int[] {
            255,
            0,
            0,
            -2147483648});
            this.nUpDw_alpha.Name = "nUpDw_alpha";
            this.nUpDw_alpha.Size = new System.Drawing.Size(57, 20);
            this.nUpDw_alpha.TabIndex = 8;
            this.nUpDw_alpha.ValueChanged += new System.EventHandler(this.nUpDw_alpha_ValueChanged);
            // 
            // lbl_alpha
            // 
            this.lbl_alpha.AutoSize = true;
            this.lbl_alpha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_alpha.Location = new System.Drawing.Point(12, 23);
            this.lbl_alpha.Name = "lbl_alpha";
            this.lbl_alpha.Size = new System.Drawing.Size(43, 16);
            this.lbl_alpha.TabIndex = 9;
            this.lbl_alpha.Text = "Alpha";
            // 
            // lbl_red
            // 
            this.lbl_red.AutoSize = true;
            this.lbl_red.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_red.ForeColor = System.Drawing.Color.Red;
            this.lbl_red.Location = new System.Drawing.Point(12, 81);
            this.lbl_red.Name = "lbl_red";
            this.lbl_red.Size = new System.Drawing.Size(34, 16);
            this.lbl_red.TabIndex = 10;
            this.lbl_red.Text = "Red";
            // 
            // lbl_green
            // 
            this.lbl_green.AutoSize = true;
            this.lbl_green.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_green.ForeColor = System.Drawing.Color.ForestGreen;
            this.lbl_green.Location = new System.Drawing.Point(12, 142);
            this.lbl_green.Name = "lbl_green";
            this.lbl_green.Size = new System.Drawing.Size(45, 16);
            this.lbl_green.TabIndex = 11;
            this.lbl_green.Text = "Green";
            // 
            // lbl_blue
            // 
            this.lbl_blue.AutoSize = true;
            this.lbl_blue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_blue.ForeColor = System.Drawing.Color.Blue;
            this.lbl_blue.Location = new System.Drawing.Point(12, 203);
            this.lbl_blue.Name = "lbl_blue";
            this.lbl_blue.Size = new System.Drawing.Size(35, 16);
            this.lbl_blue.TabIndex = 12;
            this.lbl_blue.Text = "Blue";
            // 
            // btt_undo
            // 
            this.btt_undo.Image = global::_32.Photo_Studio.Properties.Resources.undo;
            this.btt_undo.Location = new System.Drawing.Point(95, 259);
            this.btt_undo.Name = "btt_undo";
            this.btt_undo.Size = new System.Drawing.Size(35, 35);
            this.btt_undo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btt_undo.TabIndex = 13;
            this.btt_undo.TabStop = false;
            this.btt_undo.Visible = false;
            this.btt_undo.Click += new System.EventHandler(this.btt_undo_Click);
            // 
            // btt_redo
            // 
            this.btt_redo.Image = global::_32.Photo_Studio.Properties.Resources.redo;
            this.btt_redo.Location = new System.Drawing.Point(155, 259);
            this.btt_redo.Name = "btt_redo";
            this.btt_redo.Size = new System.Drawing.Size(35, 35);
            this.btt_redo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btt_redo.TabIndex = 14;
            this.btt_redo.TabStop = false;
            this.btt_redo.Visible = false;
            this.btt_redo.Click += new System.EventHandler(this.btt_redo_Click);
            // 
            // RGBSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 306);
            this.Controls.Add(this.btt_redo);
            this.Controls.Add(this.btt_undo);
            this.Controls.Add(this.lbl_blue);
            this.Controls.Add(this.lbl_green);
            this.Controls.Add(this.lbl_red);
            this.Controls.Add(this.lbl_alpha);
            this.Controls.Add(this.nUpDw_alpha);
            this.Controls.Add(this.trkBar_alpha);
            this.Controls.Add(this.nUpDw_blue);
            this.Controls.Add(this.trkBar_blue);
            this.Controls.Add(this.nUpDw_green);
            this.Controls.Add(this.trkBar_green);
            this.Controls.Add(this.btt_applica);
            this.Controls.Add(this.nUpDw_red);
            this.Controls.Add(this.trkBar_red);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RGBSelector";
            this.Text = "RGBSelector";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.trkBar_red)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDw_red)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDw_green)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkBar_green)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDw_blue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkBar_blue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trkBar_alpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUpDw_alpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_undo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btt_redo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trkBar_red;
        private System.Windows.Forms.NumericUpDown nUpDw_red;
        private System.Windows.Forms.Button btt_applica;
        private System.Windows.Forms.NumericUpDown nUpDw_green;
        private System.Windows.Forms.TrackBar trkBar_green;
        private System.Windows.Forms.NumericUpDown nUpDw_blue;
        private System.Windows.Forms.TrackBar trkBar_blue;
        private System.Windows.Forms.TrackBar trkBar_alpha;
        private System.Windows.Forms.NumericUpDown nUpDw_alpha;
        private System.Windows.Forms.Label lbl_alpha;
        private System.Windows.Forms.Label lbl_red;
        private System.Windows.Forms.Label lbl_green;
        private System.Windows.Forms.Label lbl_blue;
        private System.Windows.Forms.PictureBox btt_undo;
        private System.Windows.Forms.PictureBox btt_redo;
    }
}