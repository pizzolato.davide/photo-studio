﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _32.Photo_Studio
{
    public partial class RGBSelector : Form
    {
        public ImageContainer Parent { get; set; }

        public Effetti.Effetto effetto { get; set; }

        public RGBSelector()
        {
            InitializeComponent();
        }

        private void btt_applica_Click(object sender, EventArgs e)
        {
            int[] gain =
            {
                (int)nUpDw_alpha.Value,
                (int)nUpDw_red.Value,
                (int)nUpDw_green.Value,
                (int)nUpDw_blue.Value
            };

            Parent.Immagine.applicaEffetto(effetto, gain);
            Parent.refreshForm();
            refreshUndoRedo();
        }

        private void trkBar_alpha_Scroll(object sender, EventArgs e)
        {
            nUpDw_alpha.Value = trkBar_alpha.Value;
        }

        private void trkBar_red_Scroll(object sender, EventArgs e)
        {
            nUpDw_red.Value = trkBar_red.Value;
        }

        private void trkBar_green_Scroll(object sender, EventArgs e)
        {
            nUpDw_green.Value = trkBar_green.Value;
        }

        private void trkBar_blue_Scroll(object sender, EventArgs e)
        {
            nUpDw_blue.Value = trkBar_blue.Value;
        }

        private void nUpDw_red_ValueChanged(object sender, EventArgs e)
        {
            trkBar_red.Value = (int)nUpDw_red.Value;
        }

        private void nUpDw_green_ValueChanged(object sender, EventArgs e)
        {
            trkBar_green.Value = (int)nUpDw_green.Value;
        }

        private void nUpDw_blue_ValueChanged(object sender, EventArgs e)
        {
            trkBar_blue.Value = (int)nUpDw_blue.Value;
        }

        private void nUpDw_alpha_ValueChanged(object sender, EventArgs e)
        {
            trkBar_alpha.Value = (int)nUpDw_alpha.Value;
        }

        private void btt_undo_Click(object sender, EventArgs e)
        {
            Parent.annullaToolStripMenuItem_Click(null, null);
            refreshUndoRedo();
        }

        private void btt_redo_Click(object sender, EventArgs e)
        {
            Parent.ripristinaToolStripMenuItem_Click(null, null);
            refreshUndoRedo();
        }

        public void refreshUndoRedo()
        {
            btt_undo.Enabled = Parent.Immagine.CanAnnullare;
            btt_redo.Enabled = Parent.Immagine.CanRipetere;

            btt_undo.Visible = Parent.Immagine.CanAnnullare;
            btt_redo.Visible = Parent.Immagine.CanRipetere;
        }
    }
}
