﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace _32.Photo_Studio
{
    public static class ImageProcessor
    {
        public static Bitmap processImage(Bitmap image, Effetti.Effetto effetto, object parameter)
        {
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height; y++)
                {
                    image.SetPixel(x, y, effetto(image.GetPixel(x, y), parameter));
                }
            }
            return image;
        }
    }
}
