﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace _32.Photo_Studio
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void apri_Menu_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Image File|*.png; *.jpg; *.jpeg; *.bmp";
            openFileDialog.Title = "Apri un immagine";
            openFileDialog.RestoreDirectory = true;
            DialogResult dialogResult = openFileDialog.ShowDialog();
            if (openFileDialog.FileName != "" && dialogResult == DialogResult.OK)
            {
                Immagine immagine = new Immagine();
                immagine.Path = openFileDialog.FileName;
                Bitmap original = new Bitmap(immagine.Path);
                immagine.Image = new Bitmap(original);
                original.Dispose();
                ImageContainer newForm = new ImageContainer(immagine);
                newForm.MdiParent = this;
                newForm.Show();
                
            }
        }

        private void esci_Menu_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void informazionisuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Info info = new Info();
            info.ShowDialog();
        }
    }
}
