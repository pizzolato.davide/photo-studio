﻿namespace _32.Photo_Studio
{
    partial class ImageContainer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImageContainer));
            this.pictBox_img = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salvaconnomeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.stampaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.annullaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ripristinaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.effettiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scalaDiGrigiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.negativoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bilanciamentoColoriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.luminositàToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seppiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sogliaturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictBox_img)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictBox_img
            // 
            this.pictBox_img.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictBox_img.Location = new System.Drawing.Point(0, 25);
            this.pictBox_img.Name = "pictBox_img";
            this.pictBox_img.Size = new System.Drawing.Size(225, 229);
            this.pictBox_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictBox_img.TabIndex = 1;
            this.pictBox_img.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AllowMerge = false;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.modificaToolStripMenuItem,
            this.effettiToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(225, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.salvaToolStripMenuItem,
            this.salvaconnomeToolStripMenuItem,
            this.toolStripSeparator1,
            this.stampaToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // salvaToolStripMenuItem
            // 
            this.salvaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("salvaToolStripMenuItem.Image")));
            this.salvaToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.salvaToolStripMenuItem.Name = "salvaToolStripMenuItem";
            this.salvaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.salvaToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.salvaToolStripMenuItem.Text = "&Salva";
            this.salvaToolStripMenuItem.Click += new System.EventHandler(this.salvaToolStripMenuItem_Click);
            // 
            // salvaconnomeToolStripMenuItem
            // 
            this.salvaconnomeToolStripMenuItem.Name = "salvaconnomeToolStripMenuItem";
            this.salvaconnomeToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.salvaconnomeToolStripMenuItem.Text = "Salva &con nome";
            this.salvaconnomeToolStripMenuItem.Click += new System.EventHandler(this.salvaconnomeToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(161, 6);
            // 
            // stampaToolStripMenuItem
            // 
            this.stampaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("stampaToolStripMenuItem.Image")));
            this.stampaToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stampaToolStripMenuItem.Name = "stampaToolStripMenuItem";
            this.stampaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.stampaToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.stampaToolStripMenuItem.Text = "&Stampa";
            this.stampaToolStripMenuItem.Click += new System.EventHandler(this.stampaToolStripMenuItem_Click);
            // 
            // modificaToolStripMenuItem
            // 
            this.modificaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.annullaToolStripMenuItem,
            this.ripristinaToolStripMenuItem});
            this.modificaToolStripMenuItem.Name = "modificaToolStripMenuItem";
            this.modificaToolStripMenuItem.Size = new System.Drawing.Size(66, 20);
            this.modificaToolStripMenuItem.Text = "&Modifica";
            // 
            // annullaToolStripMenuItem
            // 
            this.annullaToolStripMenuItem.Name = "annullaToolStripMenuItem";
            this.annullaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.annullaToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.annullaToolStripMenuItem.Text = "&Annulla";
            this.annullaToolStripMenuItem.Click += new System.EventHandler(this.annullaToolStripMenuItem_Click);
            // 
            // ripristinaToolStripMenuItem
            // 
            this.ripristinaToolStripMenuItem.Name = "ripristinaToolStripMenuItem";
            this.ripristinaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.ripristinaToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
            this.ripristinaToolStripMenuItem.Text = "&Ripristina";
            this.ripristinaToolStripMenuItem.Click += new System.EventHandler(this.ripristinaToolStripMenuItem_Click);
            // 
            // effettiToolStripMenuItem
            // 
            this.effettiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scalaDiGrigiToolStripMenuItem,
            this.negativoToolStripMenuItem,
            this.bilanciamentoColoriToolStripMenuItem,
            this.luminositàToolStripMenuItem,
            this.seppiaToolStripMenuItem,
            this.sogliaturaToolStripMenuItem});
            this.effettiToolStripMenuItem.Name = "effettiToolStripMenuItem";
            this.effettiToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.effettiToolStripMenuItem.Text = "&Effetti";
            // 
            // scalaDiGrigiToolStripMenuItem
            // 
            this.scalaDiGrigiToolStripMenuItem.Name = "scalaDiGrigiToolStripMenuItem";
            this.scalaDiGrigiToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.scalaDiGrigiToolStripMenuItem.Text = "Scala di Grigi";
            this.scalaDiGrigiToolStripMenuItem.Click += new System.EventHandler(this.scalaDiGrigiToolStripMenuItem_Click);
            // 
            // negativoToolStripMenuItem
            // 
            this.negativoToolStripMenuItem.Name = "negativoToolStripMenuItem";
            this.negativoToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.negativoToolStripMenuItem.Text = "Negativo";
            this.negativoToolStripMenuItem.Click += new System.EventHandler(this.negativoToolStripMenuItem_Click);
            // 
            // bilanciamentoColoriToolStripMenuItem
            // 
            this.bilanciamentoColoriToolStripMenuItem.Name = "bilanciamentoColoriToolStripMenuItem";
            this.bilanciamentoColoriToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.bilanciamentoColoriToolStripMenuItem.Text = "Bilanciamento Colori";
            this.bilanciamentoColoriToolStripMenuItem.Click += new System.EventHandler(this.bilanciamentoColoriToolStripMenuItem_Click);
            // 
            // luminositàToolStripMenuItem
            // 
            this.luminositàToolStripMenuItem.Name = "luminositàToolStripMenuItem";
            this.luminositàToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.luminositàToolStripMenuItem.Text = "Luminosità";
            this.luminositàToolStripMenuItem.Click += new System.EventHandler(this.luminositaToolStripMenuItem_Click_1);
            // 
            // seppiaToolStripMenuItem
            // 
            this.seppiaToolStripMenuItem.Name = "seppiaToolStripMenuItem";
            this.seppiaToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.seppiaToolStripMenuItem.Text = "Seppia";
            this.seppiaToolStripMenuItem.Click += new System.EventHandler(this.seppiaToolStripMenuItem_Click);
            // 
            // sogliaturaToolStripMenuItem
            // 
            this.sogliaturaToolStripMenuItem.Name = "sogliaturaToolStripMenuItem";
            this.sogliaturaToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.sogliaturaToolStripMenuItem.Text = "Sogliatura";
            this.sogliaturaToolStripMenuItem.Click += new System.EventHandler(this.sogliaturaToolStripMenuItem_Click);
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // ImageContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(225, 254);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictBox_img);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ImageContainer";
            this.Resize += new System.EventHandler(this.ImageContainer_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.pictBox_img)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictBox_img;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salvaconnomeToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem stampaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem annullaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ripristinaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem effettiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scalaDiGrigiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem negativoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bilanciamentoColoriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem luminositàToolStripMenuItem;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.ToolStripMenuItem seppiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sogliaturaToolStripMenuItem;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
    }
}